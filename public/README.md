# pgs-campaigns

## Links
* Site Root: https://dinesh4monto.gitlab.io/pgs-campaigns/
* CiyaShop Campaign: https://dinesh4monto.gitlab.io/pgs-campaigns/ciyashop.json

Use below link to get and convert timestamp.<br>
https://www.epochconverter.com/

jSON Generator from http://www.objgen.com/json.
```
// Tabs or spaces define complex values
// Use [n] to define object arrays
campaigns[0]
  type s = image
  image s = https://www.google.com
  link s = https://www.google.com/image.jpg
  time n = 1663668000
campaigns[1]
  type s = content
  title s = Lorem Ipsum
  content s = This is a lorem ipsum. Say hello to <a href="https://www.google.com">World</a>.
  time n = 1663668000
```
